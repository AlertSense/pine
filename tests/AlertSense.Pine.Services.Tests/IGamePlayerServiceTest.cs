﻿using AlertSense.Pine.Data.Memory;
using AlertSense.Pine.Requests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Services.Tests
{
    [TestFixture]
    public class IGamePlayerServiceTest
    {
        IGamePlayerService api;
        CreatePlayerRequest createPlayerRequestOne = new CreatePlayerRequest
        {
            FirstName = "Joey",
            LastName = "Peters",
            EmailAddress = "joey@alertsense.com"
        };

        CreatePlayerRequest createPlayerRequestTwo = new CreatePlayerRequest
        {
            FirstName = "Sam",
            LastName = "Carlson",
            EmailAddress = "sam@alertsense.com"
        };

        [SetUp]
        public void Init()
        {
            api = new Api { Players = new PlayerRepository(), Games = new GameRepository() };
        }

        [Test]
        public void CreatePlayerRequest()
        {
            var playerModel = api.CreatePlayer(createPlayerRequestOne);

            Assert.IsNotNull(playerModel);
            Assert.Greater(playerModel.Id, 0);
            Assert.AreEqual(playerModel.FirstName, createPlayerRequestOne.FirstName);
            Assert.AreEqual(playerModel.LastName, createPlayerRequestOne.LastName);
            Assert.AreEqual(playerModel.EmailAddress, createPlayerRequestOne.EmailAddress);
        }

        [Test]
        public void GetPlayer()
        {

            var playerModel = api.CreatePlayer(createPlayerRequestOne);

            var retrievedPlayer = api.GetPlayer(playerModel.Id);

            Assert.IsNotNull(playerModel);
            Assert.AreEqual(playerModel.Id, retrievedPlayer.Id);
            Assert.AreEqual(playerModel.FirstName, retrievedPlayer.FirstName);
            Assert.AreEqual(playerModel.LastName, retrievedPlayer.LastName);
            Assert.AreEqual(playerModel.EmailAddress, retrievedPlayer.EmailAddress);

        }

        [Test]
        public void GetPlayers()
        {
            var firstPlayer = api.CreatePlayer(createPlayerRequestOne);
            var secondPlayer = api.CreatePlayer(createPlayerRequestTwo);

            var playerList = api.GetAllPlayers();

            Assert.IsNotNull(playerList);
            Assert.AreEqual(playerList.Count(), 2);
            Assert.AreEqual(playerList[0].Id, firstPlayer.Id);
            Assert.AreEqual(playerList[1].Id, secondPlayer.Id);
        }

        [Test]
        public void StartNewGame()
        {
            var firstPlayer = api.CreatePlayer(createPlayerRequestOne);
            var secondPlayer = api.CreatePlayer(createPlayerRequestTwo);

            var request = new CreateGameRequest
            {
                PlayerIds = new List<int> { firstPlayer.Id, secondPlayer.Id }
            };

            var createdGame = api.StartNewGame(request);

            Assert.IsNotNull(createdGame);
            Assert.Greater(createdGame.Id, 0);
            Assert.Greater(createdGame.StartTime, DateTime.MinValue);
            Assert.AreEqual(createdGame.Players[0].Id, firstPlayer.Id);
            Assert.AreEqual(createdGame.Players[1].Id, secondPlayer.Id);
        }

        [Test]
        public void GetGame()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void GetAllGames()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void CompleteGame()
        {
            var firstPlayer = api.CreatePlayer(createPlayerRequestOne);
            var secondPlayer = api.CreatePlayer(createPlayerRequestTwo);

            var request = new CreateGameRequest
            {
                PlayerIds = new List<int> { firstPlayer.Id, secondPlayer.Id }
            };

            var createdGame = api.StartNewGame(request);

            var completedRequest = new CompletedGameRequest
            {
                Id = createdGame.Id,
                PlayerIdScores = new Dictionary<int, int>
                {
                    { createdGame.Players[0].Id, 5 }, 
                    { createdGame.Players[1].Id, 11 }, 
                }
            };

            var completedGame = api.CompleteGame(completedRequest);

            Assert.IsNotNull(completedGame);
            Assert.Greater(completedGame.Id, 0);
            Assert.Greater(completedGame.StartTime, DateTime.MinValue);
            Assert.AreEqual(completedGame.PlayersScores[0].Id, firstPlayer.Id);
            Assert.AreEqual(completedGame.PlayersScores[0].Score, 5);
            Assert.AreEqual(completedGame.PlayersScores[1].Id, secondPlayer.Id);
            Assert.AreEqual(completedGame.PlayersScores[1].Score, 11);
        }        
    }
}
