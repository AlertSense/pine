﻿using AlertSense.Pine.Data.Memory;
using AlertSense.Pine.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Data
{
    [TestFixture]
    public class IGameRepositoryTest
    {
        IGameRepository games;
        IPlayerRepository players;

        Player playerJoey;
        Player playerSam;

        Game gameOne;
        Game gameTwo;

        [SetUp]
        public void Init()
        {
            games = new GameRepository();
            
            gameOne = new Game
            {
                StartTime = DateTime.Now
            };

            gameTwo = new Game
            {
                StartTime = DateTime.Now
            };


            players = new PlayerRepository();

            playerJoey = new Player
            {
                FirstName = "Joey",
                LastName = "Peters",
                EmailAddress = "joey@alertsense.com",
            };

            playerSam = new Player
            {
                FirstName = "Sam",
                LastName = "Carlson",
                EmailAddress = "sam@alertsense.com",
            };
        }

        [Test]
        public void CreateGame()
        {
            var createGame = games.Create();

            Assert.IsNotNull(createGame);
            Assert.Greater(createGame.Id, 0);
        }

        [Test]
        public void GetGame()
        {
            var createGame = games.Create();

            var retrievedGame = games.GetById(createGame.Id);

            Assert.IsNotNull(createGame);
            Assert.AreEqual(retrievedGame.Id, retrievedGame.Id);
        }

        [Test]
        public void GetGames()
        {
            var firstGame = games.Create();
            var secondGame = games.Create();

            var gameList = games.GetAll();

            Assert.IsNotNull(gameList);
            Assert.AreEqual(gameList.Count(), 2);
        }

        [Test]
        public void AddPlayerToGame()
        {
            var firstGame = games.Create();
            var firstPlayer = players.Create(playerJoey);
            var secondPlayer = players.Create(playerSam);

            var game = games.AddPlayerToGame(firstGame.Id, firstPlayer.Id);

            Assert.IsNotNull(game);
            Assert.AreEqual(game.PlayerIdScores.Count, 1);

            var gameTwo = games.AddPlayerToGame(firstGame.Id, secondPlayer.Id);

            Assert.IsNotNull(gameTwo);
            Assert.AreEqual(gameTwo.PlayerIdScores.Count, 2);

        }

        [Test]
        public void RemovePlayerFromGame()
        {
            var firstGame = games.Create();
            var firstPlayer = players.Create(playerJoey);

            var game = games.AddPlayerToGame(firstGame.Id, firstPlayer.Id);

            Assert.IsNotNull(game);
            Assert.AreEqual(game.PlayerIdScores.Count, 1);

            var gameTwo = games.RemovePlayerFromGame(firstGame.Id, firstPlayer.Id);

            Assert.IsNotNull(gameTwo);
            Assert.AreEqual(gameTwo.PlayerIdScores.Count, 0);

        }

        [Test]
        public void SetPlayerScoreForGame()
        {
            var firstGame = games.Create();
            var firstPlayer = players.Create(playerJoey);
            var secondPlayer = players.Create(playerSam);

            var game = games.AddPlayerToGame(firstGame.Id, firstPlayer.Id);

            games.SetPlayerScoreForGame(firstGame.Id, firstPlayer.Id, 11);
            

        }

        [Test]
        public void CompleteGame()
        {
            // set the completed time of the game to signify completion, verify that its set
            
        }

    }
}
