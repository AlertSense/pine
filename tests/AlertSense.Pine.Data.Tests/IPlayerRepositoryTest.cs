﻿using AlertSense.Pine.Data.Memory;
using AlertSense.Pine.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Data
{
    [TestFixture]
    public class IPlayerRepositoryTest
    {
        IPlayerRepository players;
        Player playerJoey;
        Player playerSam;

        [SetUp]
        public void Init()
        {
            players = new PlayerRepository();

            playerJoey = new Player
            {
                FirstName = "Joey",
                LastName = "Peters",
                EmailAddress = "joey@alertsense.com",
            };

            playerSam = new Player
            {
                FirstName = "Sam",
                LastName = "Carlson",
                EmailAddress = "sam@alertsense.com",
            };
        }

        [Test]
        public void CreatePlayer()
        {
            var createPlayer = players.Create(playerJoey);

            Assert.IsNotNull(createPlayer);
            Assert.Greater(createPlayer.Id, 0);
            Assert.AreEqual(createPlayer.FirstName, playerJoey.FirstName);
            Assert.AreEqual(createPlayer.LastName, playerJoey.LastName);
            Assert.AreEqual(createPlayer.EmailAddress, playerJoey.EmailAddress);
        }

        [Test]
        public void GetPlayer()
        {

            var createPlayer = players.Create(playerJoey);

            var retrievedPlayer = players.GetById(createPlayer.Id);

            Assert.IsNotNull(createPlayer);
            Assert.AreEqual(createPlayer.Id, retrievedPlayer.Id);
            Assert.AreEqual(createPlayer.FirstName, retrievedPlayer.FirstName);
            Assert.AreEqual(createPlayer.LastName, retrievedPlayer.LastName);
            Assert.AreEqual(createPlayer.EmailAddress, retrievedPlayer.EmailAddress);

        }

        [Test]
        public void GetPlayers()
        {
            var firstPlayer = players.Create(playerJoey);
            var secondPlayer = players.Create(playerSam);

            var playerList = players.GetAll();

            Assert.IsNotNull(playerList);
            Assert.AreEqual(playerList.Count(), 2);
            Assert.AreEqual(playerList[0].Id, firstPlayer.Id);
            Assert.AreEqual(playerList[1].Id, secondPlayer.Id);
        }

    }
}
