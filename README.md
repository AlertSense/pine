# Ping Pong Tracker 
## Project Codename: Pine

The ping pong tracker will be used within the AlertSense office to help keep track of ping pong games, their players, and their scores. This information will be used to rank employees by their ping pong abilities and help provide suggestions for even matched games. Leaderboards will help to encourage a healthy and competitive rivalry among employees while providing great teambuilding opportunities.

## Basic use cases

- Employees will create a player profile.
- Games will be started by selecting from a list of players and choosing to start a new game.
- A list of every game played will be displayed by the most recent game first. The finish time of the game or a status of "incomplete" will be listed for each game to help players identify which games haven't been finished from the list.
- Games can be completed once the scores have been entered for each player associated with the game.
- Players should be able to view a list of all the games they have played.

## Project Structure

### AlertSense.Pine

This root namespace level class library stores the "data contracts" that the rest of the projects utilize.

- Data
 - Interfaces for the data repositories are located here 
- Entities
 - These POCO classes are used by the repositories for passing data around. In EntityFramework these would be your code first data models.
- Models
 - These are your view model and aggregate model mapping objects. Typically these objects are the return type of api request methods.
- Requests
 - These are API request objects that can be used for MVC or down the road for a REST web api. 
- Services
 - The interfaces that define the methods that MVC or a single page app would call to interact with the business logic and data persistance.

### AlertSense.Pine.Data.Memory

This class library is a step above a mocked database. It uses dictionaries to store data in memory. There is a BaseRepositoryImpelmentation that the specific repositories use for basic crud operation and storage. This is provided to allow basic data persistance for rapid prototyping and test creation.

### AlertSense.Pine.Services

This class library provides the core business logic of the application. Here we implement IGamePlayerServices by utilitizing our various repository interfaces for data persistance. The repository interfaces are public properties allowing us to utilize dependency injection for obtaining our implementations.

### AlertSense.Pine.Website

This website is an ASP.NET MVC5 project that uses the default project MVC project template. Twitter Bootstrap CSS is used for basic styling and gives the application a polished look and feel.

The interface implementations from AlertSense.Pine.Data and AlertSense.Pine.Services are registered in the Application start in Global.asax. Dependency injection is used to allow us to easily swap between memory and SQL implementations.

The controller methods do not contain any business logic and only exist to provide data mapping, validation, and API method calls. The purpose of this design is to keep the business logic separate from the Website implementation allowing us to use ASP.NET MVC or drive a Single Page javascript app with a REST web api.


## Tests

#### AlertSense.Pine.Data.Tests

These tests verify functionality of the Data repository interfaces defined in AlertSens.Pine \Data. These tests are created to verify proper data persistance.

### AlertSense.Pine.Services.Tests

These tests verify functionality of the api interfaces defined in AlertSens.Pine \Services. These tests are created to verify business logic functionality.

In order to run NUnit tests from Visual Studio you will need the [NUnit Test Adapter](http://visualstudiogallery.msdn.microsoft.com/6ab922d0-21c0-4f06-ab5f-4ecd1fe7175d)

# TODO

### A. Design Complete Game Screen

An existing controller method on GamesController.cs needs a corresponding view created.

The view is populated by:
    
[AlertSense.Pine.Models.CompletedGameModel](src/AlertSense.Pine/Models/CompletedGameModel.cs)

The [HttpPost] method accepts:
    
[AlertSense.Pine.Requests.CompletedGameRequest](src/AlertSense.Pine/Requests/CompletedGameRequest.cs)

### B. Update Games list page

The current [Index.cshtml](src/AlertSense.Pine.Website/Views/Games/Index.cshtml) page that displays game history does not show the players scores. 

Modify the .cshtml file to display the scores along with the players names like the image below.

![](http://i.imgur.com/UKaBv0j.png)


### C. Expand Unit Test Coverage

In [AlertSense.Pine.Services.Tests.IGamePlayerServiceTest](tests/AlertSense.Pine.Services.Tests/GamePlayerServiceTest.cs), there are two unit tests that currently throw NotImplementedExceptions in:
    
    [Test]
    public void GetGame()
    {
        throw new NotImplementedException();
    }

    [Test]
    public void GetAllGames()
    {
        throw new NotImplementedException();
    }


### D. Refactor API Methods

In the implementation of IGamePlayerService, [AlertSense.Pine.Services.Api.cs](src/AlertSense.Pine.Services/Api.cs), there are two methods that have similar code.

    public CompletedGameModel GetGame(int id) // line 56

    public List<CompletedGameModel> GetAllGames() // line 78
    
These two methods smell like they violate [DRY](http://en.wikipedia.org/wiki/Don't_repeat_yourself) principles. Refactor the two methods to share their duplicated functionality.
