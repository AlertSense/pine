﻿using AlertSense.Pine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Data.Memory
{
    public class PlayerRepository : BaseMemoryRepository<Player>, IPlayerRepository
    {
    }
}
