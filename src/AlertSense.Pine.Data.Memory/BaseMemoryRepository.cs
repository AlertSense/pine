﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace AlertSense.Pine.Data.Memory
{
    public class BaseMemoryRepository<T> : IRepository<T> where T : IIdentifiable, new()
    {
        private Dictionary<int, T> data;

        public Dictionary<int, T> Data
        {
            get
            {
                if (data == null)
                    data = new Dictionary<int, T>();

                return data;
            }
            set { data = value; }
        }

        public virtual T GetById(int Id)
        {
            if (Data.ContainsKey(Id))
                return Data[Id];
            else
                throw new Exception("Data record does not exist.");
        }

        public virtual List<T> GetAll()
        {
            return Data.Values.ToList();
        }

        public virtual T Create(T entity)
        {
            var newEntity = entity.PopulateWith(entity);

            newEntity.Id = Data.Values.Count + 1;

            data.Add(entity.Id, entity);

            return entity;
        }

        public virtual T Update(T entity)
        {
            var existing = GetById(entity.Id);

            existing.PopulateWith(entity);

            return existing;
        }
    }
}
