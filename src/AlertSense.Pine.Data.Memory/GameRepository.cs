﻿using AlertSense.Pine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace AlertSense.Pine.Data.Memory
{
    public class GameRepository : BaseMemoryRepository<Game>, IGameRepository
    {
        private Dictionary<int, Dictionary<int, int>> gamesToPlayerScores;

        public Dictionary<int, Dictionary<int, int>> GamesToPlayerScores
        {
            get
            {
                if (gamesToPlayerScores == null)
                    gamesToPlayerScores = new Dictionary<int, Dictionary<int, int>>();

                return gamesToPlayerScores;
            }
            set { gamesToPlayerScores = value; }
        }

        public Game Create()
        {
            var game = Create(new Game());

            game.StartTime = DateTime.Now;

            return game;
        }

        public override Game GetById(int id)
        {
            var game = base.GetById(id);
            var newGame = new Game().PopulateWithNonDefaultValues(game);

            if (GamesToPlayerScores.ContainsKey(id))
                newGame.PlayerIdScores = GamesToPlayerScores[id];

            return newGame;
        }

        public override List<Game> GetAll()
        {
            var games = base.GetAll();

            var newGames = new List<Game>();

            foreach (var game in games)
            {
                var newGame = new Game().PopulateWithNonDefaultValues(game);

                if (GamesToPlayerScores.ContainsKey(newGame.Id))
                    newGame.PlayerIdScores = GamesToPlayerScores[newGame.Id];

                newGames.Add(newGame);
            }
            return newGames;
        }

        public Game Start(int Id)
        {
            throw new NotImplementedException();
        }

        public Game AddPlayerToGame(int gameId, int playerId)
        {
            var game = GetById(gameId);

            if (!GamesToPlayerScores.ContainsKey(gameId))
                GamesToPlayerScores.Add(gameId, new Dictionary<int, int> { { playerId, 0 } });
            else
            {
                if (GamesToPlayerScores[gameId].ContainsKey(playerId))
                    throw new Exception("Player already added to game.");
                else
                    GamesToPlayerScores[gameId].Add(playerId, 0);
            }

            return GetById(gameId);
        }

        public Game RemovePlayerFromGame(int gameId, int playerId)
        {
            var game = GetById(gameId);

            if (!GamesToPlayerScores.ContainsKey(gameId))
                throw new Exception("Game has no players to remove.");
            else
            {
                if (!GamesToPlayerScores[gameId].ContainsKey(playerId))
                    throw new Exception("Game does not contain player.");
                else
                    GamesToPlayerScores[gameId].Remove(playerId);
            }

            return GetById(gameId);
        }

        public Game SetPlayerScoreForGame(int gameId, int playerId, int score)
        {
            if (!GamesToPlayerScores.ContainsKey(gameId))
                throw new Exception("Game has no players to score.");
            else
            {
                if (!GamesToPlayerScores[gameId].ContainsKey(playerId))
                    throw new Exception("Game does not contain player.");
                else
                    GamesToPlayerScores[gameId][playerId] = score;
            }

            return GetById(gameId);
        }



        public Game Complete(int Id)
        {
            var game = base.GetById(Id);

            game.CompletedTime = DateTime.Now;

            return GetById(Id);

        }
    }
}
