﻿using AlertSense.Pine.Data;
using AlertSense.Pine.Entities;
using AlertSense.Pine.Models;
using AlertSense.Pine.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace AlertSense.Pine.Services
{
    public class Api : IGamePlayerService
    {
        public IPlayerRepository Players { get; set; }
        public IGameRepository Games { get; set; }

        public PlayerModel CreatePlayer(CreatePlayerRequest request)
        {
            var player = request.ConvertTo<Player>();
            var createdPlayer = Players.Create(player);

            return player.ConvertTo<PlayerModel>();
        }

        public List<PlayerModel> GetAllPlayers()
        {
            var players = Players.GetAll().ConvertAll(x => x.ConvertTo<PlayerModel>());

            if (players == null)
                return new List<PlayerModel>();

            return players;
        }

        public PlayerModel GetPlayer(int id)
        {
            var player = Players.GetById(id);

            return player.ConvertTo<PlayerModel>();
        }

        public StartedGameModel StartNewGame(CreateGameRequest request)
        {
            var game = Games.Create();

            var gameModel = game.ConvertTo<StartedGameModel>();

            foreach (var playerId in request.PlayerIds)
            {
                gameModel.Players.Add(GetPlayer(playerId));
                Games.AddPlayerToGame(gameModel.Id, playerId);
            }

            return gameModel;
        }
        // refactor
        public CompletedGameModel GetGame(int id)
        {
            var game = Games.GetById(id);

            var gameModel = game.ConvertTo<CompletedGameModel>();

            
            foreach (var playerScore in game.PlayerIdScores)
            {
                var playerScoreModel = GetPlayer(playerScore.Key).ConvertTo<PlayerScoreModel>();

                playerScoreModel.Score = playerScore.Value;

                gameModel.PlayersScores.Add(playerScoreModel);
            }

            return gameModel;
        }
        // refactor
        public List<CompletedGameModel> GetAllGames()
        {
            var games = Games.GetAll();

            var gameModels = games.ConvertAll(x => x.ConvertTo<CompletedGameModel>());

            for (int i = 0; i < games.Count; i++)
            {
                foreach (var playerScore in games[i].PlayerIdScores)
                {
                    var playerScoreModel = GetPlayer(playerScore.Key).ConvertTo<PlayerScoreModel>();

                    playerScoreModel.Score = playerScore.Value;

                    gameModels[i].PlayersScores.Add(playerScoreModel);
                }
            }

            if (gameModels == null)
                return new List<CompletedGameModel>();

            return gameModels;
        }

        public CompletedGameModel CompleteGame(CompletedGameRequest request)
        {
            
            var game = Games.GetById(request.Id).ConvertTo<CompletedGameModel>();

            foreach (var playerScore in request.PlayerIdScores)
            {
                var playerScoreModel = GetPlayer(playerScore.Key).ConvertTo<PlayerScoreModel>();

                playerScoreModel.Score = playerScore.Value;

                game.PlayersScores.Add(playerScoreModel);
                Games.SetPlayerScoreForGame(game.Id, playerScore.Key, playerScore.Value);
            }

            Games.Complete(request.Id);

            return game;
        }



    }
}
