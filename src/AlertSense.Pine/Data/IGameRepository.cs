﻿using AlertSense.Pine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Data
{
    public interface IGameRepository
    {
        Game Create();
        Game GetById(int id);
        List<Game> GetAll();

        Game Start(int Id);

        Game AddPlayerToGame(int gameId, int playerId);
        Game RemovePlayerFromGame(int gameId, int playerId);
        Game SetPlayerScoreForGame(int gameId, int playerId, int score);

        Game Complete(int Id);
    }
}
