﻿using AlertSense.Pine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Data
{
    public interface IPlayerRepository
    {
        Player Create(Player entity);
        Player GetById(int id);
        List<Player> GetAll();
    }
}
