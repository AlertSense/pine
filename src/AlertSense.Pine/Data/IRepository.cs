﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Data
{
    public interface IRepository<T> where T : IIdentifiable
    {
        T GetById(int Id);
        List<T> GetAll();
        T Create(T entity);
        T Update(T entity);
    }
}
