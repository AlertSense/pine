﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Entities
{
    public class Game : IIdentifiable
    {
        public int Id { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime CompletedTime { get; set; }

        public Dictionary<int,int> PlayerIdScores { get; set; }
    }  
}
