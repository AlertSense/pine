﻿using AlertSense.Pine.Models;
using AlertSense.Pine.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Services
{
    public interface IGamePlayerService
    {
        #region Player API Methods

        PlayerModel CreatePlayer(CreatePlayerRequest request);

        //PlayerModel UpdatePlayer(UpdatePlayerRequest request);

        PlayerModel GetPlayer(int id);

        List<PlayerModel> GetAllPlayers();
        
        #endregion

        #region Game API Methods

        StartedGameModel StartNewGame(CreateGameRequest request);

        CompletedGameModel GetGame(int id);

        List<CompletedGameModel> GetAllGames();

        CompletedGameModel CompleteGame(CompletedGameRequest request);

        #endregion
    }
}
