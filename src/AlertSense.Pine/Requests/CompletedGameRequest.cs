﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Requests
{
    public class CompletedGameRequest
    {
        public int Id { get; set; }
        public Dictionary<int, int> PlayerIdScores { get; set; }
    }
}
