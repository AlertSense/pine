﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Requests
{
    public class CreateGameRequest
    {
        public List<int> PlayerIds { get; set; }
    }
}
