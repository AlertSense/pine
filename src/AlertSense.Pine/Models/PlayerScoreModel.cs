﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Models
{
    public class PlayerScoreModel : PlayerModel
    {
        public int Score { get; set; }
    }
}
