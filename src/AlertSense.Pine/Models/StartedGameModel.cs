﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Models
{
    public class StartedGameModel
    {
        public int Id { get; set; }

        public DateTime StartTime { get; set; }

        public List<PlayerModel> Players { get; set; }

        public StartedGameModel()
        {
            Players = new List<PlayerModel>();
        }
    }
}
