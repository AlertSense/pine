﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSense.Pine.Models
{
    public class CompletedGameModel
    {
        public int Id { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime CompletedTime { get; set; }

        public List<PlayerScoreModel> PlayersScores { get; set; }

        public CompletedGameModel()
        {
            PlayersScores = new List<PlayerScoreModel>();
        }
    }
}
