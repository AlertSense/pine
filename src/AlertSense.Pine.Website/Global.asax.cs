﻿using AlertSense.Pine.Data.Memory;
using AlertSense.Pine.Requests;
using AlertSense.Pine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AlertSense.Pine.Website
{
    public class MvcApplication : System.Web.HttpApplication
    {
        // replace with dependency injection
        public static PlayerRepository Players = new PlayerRepository();
        public static GameRepository Games = Games = new GameRepository();
        // fake dependency injection
        public static IGamePlayerService Api = new Api { Players = Players, Games = Games };

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Api.CreatePlayer(new CreatePlayerRequest
            {
                FirstName = "Joey",
                LastName = "Peters",
                EmailAddress = "joey@alertsense.com"
            });

            Api.CreatePlayer(new CreatePlayerRequest
             {
                 FirstName = "Sam",
                 LastName = "Carlson",
                 EmailAddress = "sam@alertsense.com"
             });

        }
    }
}
