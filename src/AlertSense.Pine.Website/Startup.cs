﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AlertSense.Pine.Website.Startup))]
namespace AlertSense.Pine.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
