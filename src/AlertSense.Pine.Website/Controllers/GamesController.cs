﻿using AlertSense.Pine.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlertSense.Pine.Website.Controllers
{
    public class GamesController : Controller
    {
        public ActionResult Index()
        {
            var games = MvcApplication.Api.GetAllGames();

            return View(games);
        }

        public ActionResult Create()
        {
            var players = MvcApplication.Api.GetAllPlayers();

            ViewBag.Players = players.Select(x => new SelectListItem { Text = string.Format("{0} {1}", x.FirstName, x.LastName), Value = x.Id.ToString()});

            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateGameRequest request)
        {
            try
            {
                var gameModel = MvcApplication.Api.StartNewGame(request);

                return RedirectToAction("Edit", new { id = gameModel.Id });
            }
            catch
            {
                return View(request);
            }
        }

        public ActionResult Edit(int id)
        {
            var gameModel = MvcApplication.Api.GetGame(id);

            if (gameModel.CompletedTime != DateTime.MinValue)
                return RedirectToAction("Details", new { id = gameModel.Id });

            return View(gameModel);
        }

        [HttpPost]
        public ActionResult Edit(CompletedGameRequest request)
        {
            try
            {
                var gameModel = MvcApplication.Api.CompleteGame(request);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(request);
            }
        }

        public ActionResult Details(int id)
        {
            var gameModel = MvcApplication.Api.GetGame(id);

            if (gameModel.CompletedTime == DateTime.MinValue)
                return RedirectToAction("Edit", new { id = gameModel.Id });

            return View(gameModel);
        }
    }
}