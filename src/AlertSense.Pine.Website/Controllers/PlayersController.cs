﻿using AlertSense.Pine.Models;
using AlertSense.Pine.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceStack;

namespace AlertSense.Pine.Website.Controllers
{
    public class PlayersController : Controller
    {
        public ActionResult Index()
        {
            return View(MvcApplication.Api.GetAllPlayers());
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(CreatePlayerRequest request)
        {
            try
            {
                var playerModel = MvcApplication.Api.CreatePlayer(request);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(request);
            }
        }

        public ActionResult Edit(int id)
        {

            var playerModel = MvcApplication.Api.GetPlayer(id);

            return View(playerModel.ConvertTo<CreatePlayerRequest>());
        }

        [HttpPost]
        public ActionResult Edit(CreatePlayerRequest request)
        {
            try
            {
                var playerModel = MvcApplication.Api.CreatePlayer(request);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(request);
            }
        }

        public ActionResult Details(int id)
        {
            var playerModel = MvcApplication.Api.GetPlayer(id);

            return View(playerModel);
        }
    }
}