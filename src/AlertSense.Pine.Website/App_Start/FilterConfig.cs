﻿using System.Web;
using System.Web.Mvc;

namespace AlertSense.Pine.Website
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
